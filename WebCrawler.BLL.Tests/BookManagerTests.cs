namespace WebCrawler.BLL.Tests
{
    using WebCrawler.BLL;
    using System;
    using NUnit.Framework;
    using WebCrawler.Repository.Enum;

    [TestFixture]
    public class BookManagerTests
    {
        private BookManager _testClass;
        private RepositoryType _repositoryType;
        private string _connectionString;

        [SetUp]
        public void SetUp()
        {
            _repositoryType = RepositoryType.EF;
            _connectionString = "Data Source=INLT022;Initial Catalog=WebCrawler;Integrated Security=True";
            _testClass = new BookManager(_repositoryType, _connectionString);
        }

        [Test]
        public void CanConstruct()
        {
            var instance = new BookManager(_repositoryType, _connectionString);
            Assert.That(instance, Is.Not.Null);
        }

        [TestCase(null)]
        public void CannotConstructWithInvalidConnectionString(string value)
        {
            Assert.Throws<ArgumentNullException>(() => new BookManager(RepositoryType.EF, value));
        }

        [Test]
        public void CanCallGetAllBookSummary()
        {
            var result = _testClass.GetAllBookSummary();
            Assert.Greater(result.Count, 0);
        }

        [Test]
        public void CanCallGetLastSuccessProcessedBook()
        {
            var url = "https://www.amazon.com/Come-Up-Angie-Thomas-ebook/dp/B0716GZWJJ/ref=pd_sim_351_2/130-4090834-5776767?_encoding=UTF8&pd_rd_i=B0716GZWJJ&pd_rd_r=418b5a03-acde-4144-b59a-2e138e238403&pd_rd_w=dY75l&pd_rd_wg=XJ5Xd&pf_rd_p=9fec2710-b93d-4b3e-b3ca-e55dc1c5909a&pf_rd_r=GP137R2XVAFZXF6WSTP8&psc=1&refRID=GP137R2XVAFZXF6WSTP8";
            var result = _testClass.GetLastSuccessProcessedBook(url);
            Assert.AreEqual(22, result.ID);
        }

        [TestCase(null)]
        public void CannotCallGetLastSuccessProcessedBookWithInvalidUrl(string value)
        {
            Assert.Throws<ArgumentNullException>(() => _testClass.GetLastSuccessProcessedBook(value));
        }

        [Test]
        public void CanCallGetBookDetailInfoByCondition()
        {
            var id = 16;
            var skip = 3;
            var pageSize = 3;
            var result = _testClass.GetBookDetailInfoByCondition(id, skip, pageSize);
            Assert.AreEqual(2, result.Data.Count);
        }

        [Test]
        public void CanCallGetAllBookSummaryByCondition()
        {
            var searchValue = "TestValue1157489897";
            var sortColumn = "Url";
            var sortColumDirection = "asc";
            var skip = 2;
            var pageSize = 81427413;
            var result = _testClass.GetAllBookSummaryByCondition(searchValue, sortColumn, sortColumDirection, skip, pageSize);
            Assert.AreEqual(0, result.Data.Count);
        }

        [TestCase(null)]
        public void CannotCallGetAllBookSummaryByConditionWithInvalidSearchValue(string value)
        {
            Assert.Throws<ArgumentNullException>(() => _testClass.GetAllBookSummaryByCondition(value, "TestValue189102628", "TestValue1500723863", 722573140, 726180635));
        }

        [TestCase(null)]
        public void CannotCallGetAllBookSummaryByConditionWithInvalidSortColumn(string value)
        {
            Assert.Throws<ArgumentNullException>(() => _testClass.GetAllBookSummaryByCondition("TestValue871896222", value, "TestValue43256900", 805739332, 1431869860));
        }

        [TestCase(null)]
        public void CannotCallGetAllBookSummaryByConditionWithInvalidSortColumDirection(string value)
        {
            Assert.Throws<ArgumentException>(() => _testClass.GetAllBookSummaryByCondition("TestValue2000355728", "TestValue711047626", value, 1793674835, 492104411));
        }

        [Test]
        public void CanCallGetAllLastExecutionTime()
        {
            var result = _testClass.GetAllLastExecutionTime();
            Assert.Greater(result.Count, 0);
        }
    }
}