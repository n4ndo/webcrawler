namespace WebCrawler.Data.Tests.Models
{
    using WebCrawler.Data.Models;
    using System;
    using NUnit.Framework;

    [TestFixture]
    public class ModelBaseTests
    {
        private class TestModelBase : ModelBase
        {
        }

        private TestModelBase _testClass;

        [SetUp]
        public void SetUp()
        {
            _testClass = new TestModelBase();
        }

        [Test]
        public void CanConstruct()
        {
            var instance = new TestModelBase();
            Assert.That(instance, Is.Not.Null);
        }

        [Test]
        public void CanSetAndGetID()
        {
            var testValue = 1536830967;
            _testClass.ID = testValue;
            Assert.That(_testClass.ID, Is.EqualTo(testValue));
        }
    }
}