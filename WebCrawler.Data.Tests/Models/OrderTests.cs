namespace WebCrawler.Data.Tests.Models
{
    using WebCrawler.Data.Models;
    using System;
    using NUnit.Framework;

    [TestFixture]
    public class OrderTests
    {
        private Order _testClass;

        [SetUp]
        public void SetUp()
        {
            _testClass = new Order();
        }

        [Test]
        public void CanConstruct()
        {
            var instance = new Order();
            Assert.That(instance, Is.Not.Null);
        }
    }
}