namespace WebCrawler.Data.Tests.Models
{
    using WebCrawler.Data.Models;
    using System;
    using NUnit.Framework;

    [TestFixture]
    public class BookTests
    {
        private Book _testClass;

        [SetUp]
        public void SetUp()
        {
            _testClass = new Book();
        }

        [Test]
        public void CanConstruct()
        {
            var instance = new Book();
            Assert.That(instance, Is.Not.Null);
        }

        [Test]
        public void CanSetAndGetUrlImage()
        {
            var testValue = "TestValue659914480";
            _testClass.UrlImage = testValue;
            Assert.That(_testClass.UrlImage, Is.EqualTo(testValue));
        }

        [Test]
        public void CanSetAndGetPrice()
        {
            var testValue = "TestValue1149903620";
            _testClass.Price = testValue;
            Assert.That(_testClass.Price, Is.EqualTo(testValue));
        }

        [Test]
        public void CanSetAndGetDateCreatedUTC()
        {
            var testValue = new DateTime(512306066);
            _testClass.DateCreatedUTC = testValue;
            Assert.That(_testClass.DateCreatedUTC, Is.EqualTo(testValue));
        }
    }
}