namespace WebCrawler.Data.Tests.Models
{
    using WebCrawler.Data.Models;
    using System;
    using NUnit.Framework;

    [TestFixture]
    public class PageTests
    {
        private Page _testClass;

        [SetUp]
        public void SetUp()
        {
            _testClass = new Page();
        }

        [Test]
        public void CanConstruct()
        {
            var instance = new Page();
            Assert.That(instance, Is.Not.Null);
        }

        [Test]
        public void CanSetAndGetDescription()
        {
            var testValue = "TestValue1385056521";
            _testClass.Description = testValue;
            Assert.That(_testClass.Description, Is.EqualTo(testValue));
        }

        [Test]
        public void CanSetAndGetUrl()
        {
            var testValue = "TestValue2112021942";
            _testClass.Url = testValue;
            Assert.That(_testClass.Url, Is.EqualTo(testValue));
        }
    }
}