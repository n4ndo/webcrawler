using WebCrawler.Data.Models;

namespace WebCrawler.Data.Tests
{
    using WebCrawler.Data;
    using T = Book;
    using System;
    using NUnit.Framework;

    [TestFixture]
    public class Envelope_1Tests
    {
        private Envelope<T> _testClass;
        private T _model;

        [SetUp]
        public void SetUp()
        {
            _model = new T();
            _testClass = new Envelope<T>(_model);
        }

        [Test]
        public void CanConstruct()
        {
            var instance = new Envelope<T>(_model);
            Assert.That(instance, Is.Not.Null);
        }

        [Test]
        public void CanGetInternalType()
        {
            Assert.That(_testClass.InternalType, Is.InstanceOf<Type>());
        }

        [Test]
        public void ModelIsInitializedCorrectly()
        {
            Assert.That(_testClass.Model, Is.EqualTo(_model));
        }
    }
}