namespace WebCrawler.Data.Tests.Attributes
{
    using WebCrawler.Data.Attributes;
    using System;
    using NUnit.Framework;

    [TestFixture]
    public class XPathFieldAttributeTests
    {
        private XPathFieldAttribute _testClass;

        [SetUp]
        public void SetUp()
        {
            _testClass = new XPathFieldAttribute();
        }

        [Test]
        public void CanConstruct()
        {
            var instance = new XPathFieldAttribute();
            Assert.That(instance, Is.Not.Null);
        }

        [Test]
        public void CanSetAndGetExpression()
        {
            var testValue = new[] { "TestValue1953694245", "TestValue2066347769", "TestValue1396546649" };
            _testClass.Expression = testValue;
            Assert.That(_testClass.Expression, Is.EqualTo(testValue));
        }

        [Test]
        public void CanSetAndGetNodeAttributeValueContainer()
        {
            var testValue = "TestValue201019275";
            _testClass.NodeAttributeValueContainer = testValue;
            Assert.That(_testClass.NodeAttributeValueContainer, Is.EqualTo(testValue));
        }

        [Test]
        public void CanSetAndGetCustomHandlerName()
        {
            var testValue = "TestValue921619835";
            _testClass.CustomHandlerName = testValue;
            Assert.That(_testClass.CustomHandlerName, Is.EqualTo(testValue));
        }
    }
}