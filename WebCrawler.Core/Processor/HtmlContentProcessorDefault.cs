﻿using HtmlAgilityPack;
using System.Threading.Tasks;
using WebCrawler.Data.Models;

namespace WebCrawler.Core.Processor
{
    public class HtmlContentProcessorDefault : HtmlContentProcessorDefault<Page>
    {
        protected override Task<bool> OnBeforeProcess(HtmlDocument document)
        {
            ////Example do process here if there is a pre processing logic for handling page order
            return base.OnBeforeProcess(document);
        }

        protected override Task<Page> ProcessInternal(string url, HtmlDocument document)
        {
            ////Example do process here if there is a pre processing logic for handling page order
            return base.ProcessInternal(url, document);
        }
    }
}
