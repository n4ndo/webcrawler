﻿using HtmlAgilityPack;
using System.Threading.Tasks;
using WebCrawler.Data;

namespace WebCrawler.Core.Processor
{
    public interface IHtmlContentProcessor
    {
        Task<IModel> Process(string url, HtmlDocument document);
    }
}
