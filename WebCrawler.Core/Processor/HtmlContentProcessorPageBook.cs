﻿using System;
using System.Text.RegularExpressions;
using System.Web;
using WebCrawler.Data.Models;

namespace WebCrawler.Core.Processor
{
    public class HtmlContentProcessorPageBook : HtmlContentProcessorDefault<Book>
    {
        protected override Book CreateInnerInstance(string url)
        {
            return new Book() { DateCreatedUTC = DateTime.UtcNow, Url = url };
        }
        protected override string CustomHandlerForValue(string initialValue, string handler)
        {
            switch (handler)
            {
                case "Url":
                    string pattern = @"(http|ftp|https)://([\w_-]+(?:(?:\.[\w_-]+)+))([\w.,@?^=%&:/~+#-]*[\w@?^=%&/~+#-])?";
                    var decodevalue = HttpUtility.HtmlDecode(initialValue);
                    return Regex.Match(decodevalue, pattern).Value;
            }

            return base.CustomHandlerForValue(initialValue, handler);
        }
    }
}
