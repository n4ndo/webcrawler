﻿using HtmlAgilityPack;
using System;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using WebCrawler.Data;
using WebCrawler.Data.Attributes;
using WebCrawler.Data.Models;

namespace WebCrawler.Core.Processor
{
    public abstract class HtmlContentProcessorDefault<T> : IHtmlContentProcessor where T : Page, IModel
    {
        public Task<IModel> Process(string url, HtmlDocument document)
        {
            OnBeforeProcess(document);
            T result = ProcessInternal(url,document).Result;
            OnAfterProcess(document);

            return Task.FromResult((IModel)result);
        }

        protected virtual T CreateInnerInstance(string url)
        {
            if(url == null)
            {
                throw new ArgumentNullException();
            }

            T result = (T)Activator.CreateInstance(typeof(T));
            result.Url = url;
            return result;
        }
        protected virtual Task<bool> OnBeforeProcess(HtmlDocument document) 
        {
            if (document == null)
            {
                throw new ArgumentNullException();
            }
            return Task.FromResult(true); 
        }
        protected virtual Task<bool> OnAfterProcess(HtmlDocument document) 
        {
            if(document == null)
            {
                throw new ArgumentNullException();
            }
            return Task.FromResult(true); 
        }
        protected virtual Task<T> ProcessInternal(string url, HtmlDocument document)
        {
            if(url == null || document == null)
            {
                throw new ArgumentNullException();
            }

            ////Processing dynamically using reflection (the default can handle multiple type)
            T result = CreateInnerInstance(url);
            PropertyInfo[] props = (typeof(T)).GetProperties();
            foreach (var prop in props)
            {
                XPathFieldAttribute attribute = prop.GetCustomAttribute<XPathFieldAttribute>();
                if (attribute != null)
                {
                    string value = string.Empty;
                    foreach(var item in attribute.Expression)
                    {
                        HtmlNode node = document.DocumentNode.SelectSingleNode(item);
                        if(node != null)
                        {
                            value = node.InnerText;
                            break;
                        }
                    }
                    
                    if (!string.IsNullOrWhiteSpace(attribute.NodeAttributeValueContainer))
                    {
                        foreach(var item in attribute.Expression)
                        {
                            HtmlNode node = document.DocumentNode.SelectSingleNode(item);
                            if(node != null)
                            {
                                value = node.GetAttributeValue(attribute.NodeAttributeValueContainer, "");
                                break;
                            }
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(attribute.CustomHandlerName))
                    {
                        value = CustomHandlerForValue(value, attribute.CustomHandlerName);
                    }

                    value = Regex.Replace(value, @"\s+", string.Empty);
                    prop.SetValue(result, value);
                }
            }

            return Task.FromResult(result);
        }

        protected virtual string CustomHandlerForValue(string initialValue, string handler)
        {
            if(initialValue == null || handler == null)
            {
                throw new ArgumentNullException();
            }

            return initialValue;
        }
    }
}
