﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WebCrawler.Core.BackgroundTask
{
    public class PeriodicTask
    {
        //keep a list of scheduled actions
        static Dictionary<Func<Task>, CancellationTokenSource> _scheduledActions = new Dictionary<Func<Task>, CancellationTokenSource>();

        //schedule an action
        public static async Task Run(Func<Task> action, TimeSpan period)
        {
            if (_scheduledActions.ContainsKey(action)) return; //this action is already scheduled, get out

            var cancellationTokenSrc = new CancellationTokenSource(); //create cancellation toket so we can abort later when we want

            _scheduledActions.Add(action, cancellationTokenSrc); //save it all into our disctionary

            //main scheduling loop
            Task task = null;
            while (!cancellationTokenSrc.IsCancellationRequested)
            {
                if (task == null || task.IsCompleted) //skip if previous invocation is still running (our requirements allow that)
                    task = action();
                await Task.Delay(period, cancellationTokenSrc.Token);
            }
        }

        public static void Stop(Func<Task> action)
        {
            if (!_scheduledActions.ContainsKey(action)) return; //this method has not been scheduled, get out
            _scheduledActions[action].Cancel(); //stop the task
            _scheduledActions.Remove(action); //remove from the collection
        }
    }
}
