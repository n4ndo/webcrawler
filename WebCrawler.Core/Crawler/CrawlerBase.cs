﻿using System;
using System.Threading.Tasks;
using WebCrawler.Core.Downloader;
using WebCrawler.Core.Factory;
using WebCrawler.Core.Processor;
using WebCrawler.Data;
using WebCrawler.Repository;
using WebCrawler.Repository.Enum;
using WebCrawler.Repository.Factory;

namespace WebCrawler.Core.Crawler
{
    public class CrawlerBase<T> : ICrawler where T : class, IModel
    {
        private IDownloader Downloader { get; set; }
        private IHtmlContentProcessor Processor { get; set; }
        private IRepository<T> Repository { get; set; }
        public CrawlerBase(IDownloader downloader, IHtmlContentProcessor processor, RepositoryType repositoryType, string connectionString)
        {
            if(downloader == null || processor == null || connectionString == null){
                throw new ArgumentNullException();
            }

            Downloader = downloader;
            Processor = processor;
            Repository = RepositoryFactory<T>.Create(repositoryType, connectionString);
        }

        public CrawlerBase(IDownloader downloader, IHtmlContentProcessorFactory processorFactory, RepositoryType repositoryType, string connectionString)
        {
            if (downloader == null || processorFactory == null || connectionString == null)
            {
                throw new ArgumentNullException();
            }

            Downloader = downloader;
            Processor = processorFactory.Create<T>();
            Repository = RepositoryFactory<T>.Create(repositoryType, connectionString);
        }

        public virtual async Task<bool> Process(string url)
        {
            var document = await Downloader.Process(url).ConfigureAwait(false);
            var entity = await Processor.Process(url, document).ConfigureAwait(false);
            return await Repository.Add((T)entity).ConfigureAwait(false);
        }
    }
}
