﻿using System.Threading.Tasks;

namespace WebCrawler.Core.Crawler
{
    public interface ICrawler
    {
        Task<bool> Process(string url);
    }
}
