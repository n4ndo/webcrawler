﻿using HtmlAgilityPack;
using System.Threading.Tasks;

namespace WebCrawler.Core.Downloader
{
    public interface IDownloader
    {
        Task<HtmlDocument> Process(string url);
    }
}
