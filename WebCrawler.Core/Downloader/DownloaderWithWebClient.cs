﻿using HtmlAgilityPack;
using System.Net;
using System.Threading.Tasks;

namespace WebCrawler.Core.Downloader
{
    public class DownloaderWithWebClient : DownloaderBase
    {
        public WebClient Client { get; private set; }
        public DownloaderWithWebClient(WebClient client) : base()
        {
            Client = client;
        }

        public override async Task<HtmlDocument> Process(string url)
        {
            HtmlDocument document = new HtmlDocument();
            string htmlText = await Client.DownloadStringTaskAsync(url).ConfigureAwait(false);
            document.LoadHtml(htmlText);

            return document;
        }
    }
}
