﻿using HtmlAgilityPack;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace WebCrawler.Core.Downloader
{
    public class DownloaderWithHttpClient : DownloaderBase
    {
        public HttpClient Client { get; private set; }
        public DownloaderWithHttpClient(HttpClient client) : base()
        {
            if(client == null)
            {
                throw new ArgumentNullException();
            }
            Client = client;
        }

        public override async Task<HtmlDocument> Process(string url)
        {
            if(url == null)
            {
                throw new ArgumentNullException();
            }

            HtmlDocument document = new HtmlDocument();
            using (HttpResponseMessage response = await Client.GetAsync(url).ConfigureAwait(false))
            {
                using (HttpContent content = response.Content)
                {
                    string result = await content.ReadAsStringAsync().ConfigureAwait(false);
                    document.LoadHtml(result);
                }
            }

            return document;
        }
    }
}
