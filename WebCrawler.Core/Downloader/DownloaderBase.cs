﻿using HtmlAgilityPack;
using System;
using System.Threading.Tasks;

namespace WebCrawler.Core.Downloader
{
    public abstract class DownloaderBase : IDownloader
    {
        public DownloaderBase() { }
        public virtual Task<HtmlDocument> Process(string url)
        {
            if(url == null)
            {
                throw new ArgumentNullException();
            }
            return Task.FromResult(new HtmlDocument());
        }
    }
}
