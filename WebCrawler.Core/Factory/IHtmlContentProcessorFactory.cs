﻿using System;
using System.Collections.Generic;
using System.Text;
using WebCrawler.Core.Processor;
using WebCrawler.Data;

namespace WebCrawler.Core.Factory
{
    public interface IHtmlContentProcessorFactory
    {
        IHtmlContentProcessor Create<T>() where T : IModel;
    }
}
