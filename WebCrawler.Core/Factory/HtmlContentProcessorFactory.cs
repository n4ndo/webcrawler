﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using WebCrawler.Core.Processor;
using WebCrawler.Data;

namespace WebCrawler.Core.Factory
{
    public class HtmlContentProcessorFactory : IHtmlContentProcessorFactory
    {
        private Dictionary<string, IHtmlContentProcessor> specialProcessorCollection = new Dictionary<string, IHtmlContentProcessor>();

        public HtmlContentProcessorFactory()
        {
            ////Register here if has other special processor
            specialProcessorCollection.Add("WebCrawler.Data.Models.Book", new HtmlContentProcessorPageBook());
        }

        protected virtual void AddSpecialProcessorCollection(Dictionary<string, IHtmlContentProcessor> specialProcessorCollection) { }

        public IHtmlContentProcessor Create<T>() where T : IModel
        {
            IHtmlContentProcessor result = null;
            string key = typeof(T).FullName;
            if (specialProcessorCollection.ContainsKey(key))
            {
                specialProcessorCollection.TryGetValue(key, out result);
            }
            else
            {
                result = new HtmlContentProcessorDefault();
            }

            return result;
        }
    }
}
