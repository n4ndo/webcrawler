﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using WebCrawler.BLL;
using WebCrawler.Core.BackgroundTask;
using WebCrawler.Core.Crawler;
using WebCrawler.Core.Downloader;
using WebCrawler.Core.Processor;
using WebCrawler.Data;
using WebCrawler.Data.Models;
using WebCrawler.Repository;
using WebCrawler.Utility;

namespace WebCrawler.Dummy
{
    class Program
    {
        static void Main(string[] args)
        {
            //PeriodicTask.Run(() => QueueWebCrawler(), TimeSpan.FromMinutes(5)).FireAndForget();

            IDownloader downloder = new DownloaderWithHttpClient(new HttpClient(
                new HttpClientHandler() {
                    AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate 
                }));
            IHtmlContentProcessor processorDefault = new HtmlContentProcessorDefault();
            IHtmlContentProcessor processorBook = new HtmlContentProcessorPageBook();

            string connectionString = "Data Source=INLT022;Initial Catalog=WebCrawler;Integrated Security=True";
            CrawlerBase<Book> crawlerBook = new CrawlerBase<Book>(downloder, processorBook, Repository.Enum.RepositoryType.EF, connectionString);
            crawlerBook.Process("https://www.amazon.com/dp/1644450003/ref=s9_acsd_al_bw_c2_x_0_i?pf_rd_m=ATVPDKIKX0DER&pf_rd_s=merchandised-search-3&pf_rd_r=1ZZSH2X2XHQW58M96Y4T&pf_rd_t=101&pf_rd_p=c78a9dc1-c2dc-497f-9238-f54789994f24&pf_rd_i=6960520011").GetAwaiter().GetResult();

            crawlerBook.Process("https://www.amazon.com/Tattooist-Auschwitz-Novel-Heather-Morris-ebook/dp/B0756DZ4C1/ref=pd_vtpd_14_4/130-4090834-5776767?_encoding=UTF8&pd_rd_i=B0756DZ4C1&pd_rd_r=a39cdcc4-fd41-44a1-a00e-64a759c14094&pd_rd_w=5bVh3&pd_rd_wg=BzLTU&pf_rd_p=be9253e2-366d-447b-83fa-e044efea8928&pf_rd_r=RR7V8T6SZ1831VWXMR5G&psc=1&refRID=RR7V8T6SZ1831VWXMR5G").GetAwaiter().GetResult();

            //CrawlerBase<Order> crawlerOrder = new CrawlerBase<Order>(downloder, processorDefault, repository);
            //crawlerOrder.Process("http://test.com/Order").GetAwaiter().GetResult();
        }

        private static async Task QueueWebCrawler()
        {
            await Task.Delay(5000);
        }
    }
}
