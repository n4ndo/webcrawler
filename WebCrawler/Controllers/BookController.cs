﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebCrawler.BLL;
using WebCrawler.BLL.ViewModel;
using WebCrawler.Core.Crawler;
using WebCrawler.Data.Models;

namespace WebCrawler.Controllers
{
    public class BookController : Controller
    {
        private BookManager _manager;
        private CrawlerBase<Book> _crawler;
        public BookController(BookManager manager, CrawlerBase<Book> crawler)
        {
            _manager = manager;
            _crawler = crawler;
        }
        public IActionResult Index()
        {
            return View();
        }

        public ActionResult LoadData()
        {
            //Get Request Data
            var draw = Request.Form.Where(s => s.Key.Equals("draw", StringComparison.OrdinalIgnoreCase)).Select(s => s.Value).FirstOrDefault();
            var start = Request.Form.Where(s => s.Key.Equals("start", StringComparison.OrdinalIgnoreCase)).Select(s => s.Value).FirstOrDefault();
            var length = Request.Form.Where(s => s.Key.Equals("length", StringComparison.OrdinalIgnoreCase)).Select(s => s.Value).FirstOrDefault();
            var columnName = Request.Form.Where(h => h.Key.Equals("order[0][column]", StringComparison.OrdinalIgnoreCase)).Select(s => s.Value).FirstOrDefault();
            var sortColumn = Request.Form.Where(s => s.Key.Equals(string.Format("columns[{0}][name]", columnName))).Select(s => s.Value).FirstOrDefault();
            var sortColumnDir = Request.Form.Where(s => s.Key.Equals("order[0][dir]", StringComparison.OrdinalIgnoreCase)).Select(s => s.Value).FirstOrDefault();
            var searchValue = Request.Form.Where(s => s.Key.Equals("search[value]", StringComparison.OrdinalIgnoreCase)).Select(s => s.Value).FirstOrDefault();

            //Paging Size (10,20,50,100)  
            int pageSize = !string.IsNullOrEmpty(length) ? Convert.ToInt32(length) : 0;
            int skip = string.IsNullOrEmpty(start) ? Convert.ToInt32(start) : 0;

            // Getting all Book data  
            var bookData = _manager.GetAllBookSummaryByCondition(searchValue, sortColumn, sortColumnDir, skip, pageSize);

            return Json(new { draw = draw, recordsFiltered = bookData.countTotal, recordsTotal = bookData.countTotal, data = bookData.Data });
        }

        [HttpGet]
        public ActionResult New()
        {
            PageBookViewModel viewModel = new PageBookViewModel();
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Save(PageBookViewModel book)
        {
            try
            {
                bool status = await _crawler.Process(book.Url);
                if (status)
                {
                    var bookresult = _manager.GetLastSuccessProcessedBook(book.Url);
                    return RedirectToAction("ViewDetail",new { id = bookresult.ID});
                }
                else
                {
                    return View(book);
                }
            }catch
            {
                return View(book);
            }
        }

        [HttpGet]
        public ActionResult ViewDetail(int id)
        {
            ViewBag.SelectedID = id;
            return View();
        }

        public ActionResult LoadDataDetail(int id)
        {
            //Get Request Data
            var draw = Request.Form.Where(s => s.Key.Equals("draw", StringComparison.OrdinalIgnoreCase)).Select(s => s.Value).FirstOrDefault();
            var start = Request.Form.Where(s => s.Key.Equals("start", StringComparison.OrdinalIgnoreCase)).Select(s => s.Value).FirstOrDefault();
            var length = Request.Form.Where(s => s.Key.Equals("length", StringComparison.OrdinalIgnoreCase)).Select(s => s.Value).FirstOrDefault();

            //Paging Size (10,20,50,100)  
            int pageSize = !string.IsNullOrEmpty(length) ? Convert.ToInt32(length) : 0;
            int skip = string.IsNullOrEmpty(start) ? Convert.ToInt32(start) : 0;

            // Getting all detail Book data  
            var bookDetails = _manager.GetBookDetailInfoByCondition(id, skip, pageSize);

            return Json(new { draw = draw, recordsFiltered = bookDetails.countTotal, recordsTotal = bookDetails.countTotal, data = bookDetails.Data });
        }
    }
}