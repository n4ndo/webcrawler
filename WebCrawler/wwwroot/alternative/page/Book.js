﻿$(document).ready(function () {

    var oTable = $("#BookGrid").DataTable({

        "processing": true, // for show progress bar
        "serverSide": true, // for process server side
        "filter": true, // this is for disable filter (search box)
        "orderMulti": false, // for disable multiple column at once
        "pageLength": 10,

        "ajax": {
            "url": "/Book/LoadData",
            "type": "POST",
            "datatype": "json"
        },

        "columnDefs":
        [{
            "targets": [0],
            "visible": false,
            "searchable": false
        },
        {
            "targets": [4],
            "searchable": false,
            "orderable": false
        }],

        "columns": [
              { "data": "id", "name": "ID", "autoWidth": true },
              { "data": "url", "name": "Url", "autoWidth": true },
              { "data": "description", "name": "Description", "autoWidth": true },
              { "data": "latestPrice", "name": "LatestPrice", "autoWidth": true },
              {
                  "render": function (data, type, full, meta) {
                      return '<a class="btn btn-info" href="/Book/ViewDetail/' + full.id + '">View</a>';
                  }
              },
        ]

    });

    var selectedID = $("#selectedDetailID").val();
    var oTableDetail = $("#BookDetailGrid").DataTable({

        "processing": true, // for show progress bar
        "serverSide": true, // for process server side
        "filter": false, // this is for disable filter (search box)
        "orderMulti": false, // for disable multiple column at once
        "pageLength": 10,

        "ajax": {
            "url": "/Book/LoadDataDetail/" + selectedID,
            "type": "POST",
            "datatype": "json"
        },

        "columnDefs":
            [{
                "targets": [0],
                "searchable": false,
                "orderable": false
            },
            {
                "targets": [1],
                "searchable": false,
                "orderable": false,
                render: function (data) {
                    return "<img src='" + data + "' height='80' width='80' />";
                }
            },
            {
                "targets": [2],
                "searchable": false,
                "orderable": false
            },
            {
                "targets": [3],
                "searchable": false,
                "orderable": false
                ,render: function (data) {
                    return moment(data).format('YYYY-MM-DD HH:mm:ss');
                }
            }],

        "columns": [
            { "data": "description", "name": "Description", "autoWidth": true },
            { "data": "urlImage", "name": "UrlImage", "autoWidth": true },
            { "data": "price", "name": "Price", "autoWidth": true },
            { "data": "dateCreatedUTC", "name": "DateCreatedUTC", "autoWidth": true }
        ]

    });

    $('.tablecontainer').on('click', 'a.btn-info', function (e) {
        e.preventDefault();
        document.location = $(this).attr('href');
    })

    $('.tablecontainer').on('click', 'a.popup', function (e) {
        e.preventDefault();
        OpenPopup($(this).attr('href'));
        //document.location = $(this).attr('href');
    })

    function OpenPopup(pageUrl) {
        var $pageContent = $('<div/>');
        $pageContent.load(pageUrl, function () {
            $('#popupForm', $pageContent).removeData('validator');
            $('#popupForm', $pageContent).removeData('unobtrusiveValidation');
        });

        $dialog = $('<div class="popupWindow" style="overflow:auto"></div>')
            .html($pageContent)
            .dialog({
                draggable: false,
                autoOpen: false,
                resizable: false,
                model: true,
                title: 'Popup Dialog',
                height: 550,
                width: 600,
                close: function () {
                    $dialog.dialog('destroy').remove();
                }
            })

        $('.popupWindow').on('submit', '#popupForm', function (e) {
            var url = $('#popupForm')[0].action;
            $.ajax({
                type: "POST",
                url: url,
                data: $('#popupForm').serialize(),
                success: function (data) {
                    if (data.status) {
                        $dialog.dialog('close');
                        oTable.ajax.reload();
                    }
                    else {
                        alert(data.message)
                    }
                }
            })

            e.preventDefault();
        })

        $dialog.dialog('open');
    }

});

function getImg(data, type, full, meta) {
    var orderType = data.OrderType;
    if (orderType === 'Surplus') {
        return '<img src="image path here" />';
    } else {
        return '<img src="image path here" />';
    }
}
