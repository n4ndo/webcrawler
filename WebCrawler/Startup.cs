using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using WebCrawler.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Authorization;
using WebCrawler.Repository.EF;
using Autofac;
using System.Net.Http;
using System.Net;
using WebCrawler.Core.Downloader;
using WebCrawler.Core.Processor;
using WebCrawler.Core.Crawler;
using WebCrawler.Data.Models;
using WebCrawler.BLL;
using WebCrawler.Core.BackgroundTask;
using System.Threading;
using Autofac.Extensions.DependencyInjection;
using WebCrawler.BLL.ViewModel;
using WebCrawler.Core.Factory;

namespace WebCrawler
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public ILifetimeScope AutofacContainer { get; private set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("DefaultConnection")));
            services.AddDefaultIdentity<IdentityUser>(options => options.SignIn.RequireConfirmedAccount = false)
                .AddEntityFrameworkStores<ApplicationDbContext>();
            services.AddControllersWithViews();
            services.AddRazorPages().AddRazorRuntimeCompilation();

            services.AddMvc(o =>
            {
                var policy = new AuthorizationPolicyBuilder()
                    .RequireAuthenticatedUser()
                    .Build();
                o.Filters.Add(new AuthorizeFilter(policy));
            });

            //services.AddDbContext<WebCrawlerDbContext>(options =>
            //    options.UseSqlServer(
            //        Configuration.GetConnectionString("DefaultConnection")));
        }

        // ConfigureContainer is where you can register things directly
        // with Autofac. This runs after ConfigureServices so the things
        // here will override registrations made in ConfigureServices.
        // Don't build the container; that gets done for you by the factory.
        public void ConfigureContainer(ContainerBuilder builder)
        {
            // Register your own things directly with Autofac, like:
            builder.Register(s =>
                new HttpClient(
                    new HttpClientHandler()
                    {
                        AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate
                    })).Named<HttpClient>("GZIPHttpClient").SingleInstance();

            builder.Register(s => new DownloaderWithHttpClient(s.ResolveKeyed<HttpClient>("GZIPHttpClient"))).Named<IDownloader>("DownloaderClient").SingleInstance();

            ////if want to use ContentProcessor Factory
            //builder.RegisterType<HtmlContentProcessorFactory>().As<IHtmlContentProcessorFactory>().SingleInstance();
            //builder.Register(s => new CrawlerBase<Book>(
            //        s.ResolveNamed<IDownloader>("DownloaderClient"),
            //        s.Resolve<IHtmlContentProcessorFactory>(),
            //        Repository.Enum.RepositoryType.EF,
            //        Configuration.GetConnectionString("DefaultConnection")
            //    )).Named<ICrawler>("BookCrawler").SingleInstance();
            //builder.Register(s => new CrawlerBase<Book>(
            //        s.ResolveNamed<IDownloader>("DownloaderClient"),
            //        s.Resolve<IHtmlContentProcessorFactory>(),
            //        Repository.Enum.RepositoryType.EF,
            //        Configuration.GetConnectionString("DefaultConnection")
            //    )).SingleInstance();

            ////This time use Explicit type of ContentProcessor
            builder.RegisterType<HtmlContentProcessorPageBook>().As<IHtmlContentProcessor>().SingleInstance();
            builder.Register(s => new CrawlerBase<Book>(
                    s.ResolveNamed<IDownloader>("DownloaderClient"),
                    s.Resolve<IHtmlContentProcessor>(),
                    Repository.Enum.RepositoryType.EF,
                    Configuration.GetConnectionString("DefaultConnection")
                )).Named<ICrawler>("BookCrawler").SingleInstance();
            builder.Register(s => new CrawlerBase<Book>(
                    s.ResolveNamed<IDownloader>("DownloaderClient"),
                    s.Resolve<IHtmlContentProcessor>(),
                    Repository.Enum.RepositoryType.EF,
                    Configuration.GetConnectionString("DefaultConnection")
                )).SingleInstance();

            builder.Register(s => new BookManager(Repository.Enum.RepositoryType.EF, Configuration.GetConnectionString("DefaultConnection")))
                .Named("BookManager", typeof(BookManager)).SingleInstance();
            builder.Register(s => new BookManager(Repository.Enum.RepositoryType.EF, Configuration.GetConnectionString("DefaultConnection")))
                .SingleInstance();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            // If, for some reason, you need a reference to the built container, you
            // can use the convenience extension method GetAutofacRoot.
            AutofacContainer = app.ApplicationServices.GetAutofacRoot();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapRazorPages();
            });

            var cts = new CancellationTokenSource();
            Task t = Task.Factory.StartNew(
            async () =>
            {
                await PeriodicTask.Run(QueueWebCrawler, TimeSpan.FromMinutes(2));
            }, cts.Token, TaskCreationOptions.LongRunning, TaskScheduler.Default).Unwrap();

        }

        private async Task QueueWebCrawler()
        {
            CrawlerBase<Book> crawler = AutofacContainer.ResolveNamed<ICrawler>("BookCrawler") as CrawlerBase<Book>;
            BookManager bookManager = AutofacContainer.ResolveNamed<BookManager>("BookManager");
            List<PageBookViewModel> pageBooks = bookManager.GetAllLastExecutionTime();
            pageBooks = pageBooks.Where(s => s.LastExecutedTimeUTC.AddHours(1) <= DateTime.UtcNow).ToList();

            foreach(var item in pageBooks)
            {
                await crawler.Process(item.Url);
            }
        }
    }
}
