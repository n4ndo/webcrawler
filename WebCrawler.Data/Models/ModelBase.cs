﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebCrawler.Data.Models
{
    public abstract class ModelBase : IModel
    {
        public int ID { get; set; }
    }
}
