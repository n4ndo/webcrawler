﻿using System;
using System.Collections.Generic;
using System.Text;
using WebCrawler.Data.Attributes;

namespace WebCrawler.Data.Models
{
    public class Book : Page
    {
        [XPathFieldAttribute(Expression = new string[2] { "//*[@id='imgBlkFront']", "//*[@id='ebooksImgBlkFront']" },NodeAttributeValueContainer = "data-a-dynamic-image", CustomHandlerName ="Url")]
        public string UrlImage { get; set; }
        [XPathFieldAttribute(Expression = new string[1] { "//a[@class='a-button-text']/span[contains(@class,'a-color')][1]" })]
        public string Price { get; set; }
        public DateTime DateCreatedUTC { get; set; }
    }
}
