﻿using System;
using System.Collections.Generic;
using System.Text;
using WebCrawler.Data.Attributes;

namespace WebCrawler.Data.Models
{
    public class Page : ModelBase
    {
        [XPathFieldAttribute(Expression = new string[] { "//*[@id='productTitle']/text()" })]
        public string Description { get; set; }
        public string Url { get; set; }
    }
}
