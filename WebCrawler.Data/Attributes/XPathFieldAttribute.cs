﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebCrawler.Data.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class XPathFieldAttribute : Attribute
    {
        public string[] Expression { get; set; }
        public string NodeAttributeValueContainer { get; set; }
        public string CustomHandlerName { get; set; }
    }
}
