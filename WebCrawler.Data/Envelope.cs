﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebCrawler.Data
{
    public class Envelope<T> where T : IModel
    {
        public Type InternalType { get; private set; }
        public T Model { get; private set; }
        public Envelope(T model)
        {
            InternalType = model.GetType();
            Model = model;
        }
        
    }
}
