namespace WebCrawler.Core.Tests.Processor
{
    using WebCrawler.Core.Processor;
    using System;
    using NUnit.Framework;

    [TestFixture]
    public class HtmlContentProcessorPageBookTests
    {
        private HtmlContentProcessorPageBook _testClass;

        [SetUp]
        public void SetUp()
        {
            _testClass = new HtmlContentProcessorPageBook();
        }

        [Test]
        public void CanConstruct()
        {
            var instance = new HtmlContentProcessorPageBook();
            Assert.That(instance, Is.Not.Null);
        }
    }
}