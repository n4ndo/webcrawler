namespace WebCrawler.Core.Tests.Processor
{
    using WebCrawler.Core.Processor;
    using System;
    using NUnit.Framework;

    [TestFixture]
    public class HtmlContentProcessorDefaultTests
    {
        private HtmlContentProcessorDefault _testClass;

        [SetUp]
        public void SetUp()
        {
            _testClass = new HtmlContentProcessorDefault();
        }

        [Test]
        public void CanConstruct()
        {
            var instance = new HtmlContentProcessorDefault();
            Assert.That(instance, Is.Not.Null);
        }
    }
}