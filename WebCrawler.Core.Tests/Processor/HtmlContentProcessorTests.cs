using WebCrawler.Data.Models;

namespace WebCrawler.Core.Tests.Processor
{
    using WebCrawler.Core.Processor;
    using T = Book;
    using System;
    using NUnit.Framework;
    using HtmlAgilityPack;
    using System.Threading.Tasks;

    [TestFixture]
    public class HtmlContentProcessorDefault_1Tests
    {
        private class TestHtmlContentProcessorDefault : HtmlContentProcessorDefault<T>
        {
            public T PublicCreateInnerInstance(string url)
            {
                return base.CreateInnerInstance(url);
            }

            public Task<bool> PublicOnBeforeProcess(HtmlDocument document)
            {
                return base.OnBeforeProcess(document);
            }

            public Task<bool> PublicOnAfterProcess(HtmlDocument document)
            {
                return base.OnAfterProcess(document);
            }

            public Task<T> PublicProcessInternal(string url, HtmlDocument document)
            {
                return base.ProcessInternal(url, document);
            }

            public string PublicCustomHandlerForValue(string initialValue, string handler)
            {
                return base.CustomHandlerForValue(initialValue, handler);
            }
        }

        private TestHtmlContentProcessorDefault _testClass;

        [SetUp]
        public void SetUp()
        {
            _testClass = new TestHtmlContentProcessorDefault();
        }

        [Test]
        public void CanConstruct()
        {
            var instance = new TestHtmlContentProcessorDefault();
            Assert.That(instance, Is.Not.Null);
        }

        [Test]
        public async Task CanCallProcess()
        {
            var url = "TestValue1060323791";
            var document = new HtmlDocument();
            var result = await _testClass.Process(url, document);
            Assert.IsNotNull(result);
        }

        [Test]
        public void CannotCallProcessWithNullDocument()
        {
            Assert.ThrowsAsync<ArgumentNullException>(() => _testClass.Process("TestValue119591862", default(HtmlDocument)));
        }

        [TestCase(null)]
        public void CannotCallProcessWithInvalidUrl(string value)
        {
            Assert.ThrowsAsync<ArgumentNullException>(() => _testClass.Process(value, new HtmlDocument()));
        }

        [Test]
        public void CanCallCreateInnerInstance()
        {
            var url = "TestValue732517207";
            var result = _testClass.PublicCreateInnerInstance(url);
            Assert.IsNotNull(result);
        }

        [TestCase(null)]
        public void CannotCallCreateInnerInstanceWithInvalidUrl(string value)
        {
            Assert.Throws<ArgumentNullException>(() => _testClass.PublicCreateInnerInstance(value));
        }

        [Test]
        public async Task CanCallOnBeforeProcess()
        {
            var document = new HtmlDocument();
            var result = await _testClass.PublicOnBeforeProcess(document);
            Assert.AreEqual(true, result);
        }

        [Test]
        public void CannotCallOnBeforeProcessWithNullDocument()
        {
            Assert.ThrowsAsync<ArgumentNullException>(() => _testClass.PublicOnBeforeProcess(default(HtmlDocument)));
        }

        [Test]
        public async Task CanCallOnAfterProcess()
        {
            var document = new HtmlDocument();
            var result = await _testClass.PublicOnAfterProcess(document);
            Assert.AreEqual(true, result);
        }

        [Test]
        public void CannotCallOnAfterProcessWithNullDocument()
        {
            Assert.ThrowsAsync<ArgumentNullException>(() => _testClass.PublicOnAfterProcess(default(HtmlDocument)));
        }

        [Test]
        public async Task CanCallProcessInternal()
        {
            var url = "TestValue1306674264";
            var document = new HtmlDocument();
            var result = await _testClass.PublicProcessInternal(url, document);
            Assert.IsNotNull(result);
        }

        [Test]
        public void CannotCallProcessInternalWithNullDocument()
        {
            Assert.ThrowsAsync<ArgumentNullException>(() => _testClass.PublicProcessInternal("TestValue427330480", default(HtmlDocument)));
        }

        [TestCase(null)]
        public void CannotCallProcessInternalWithInvalidUrl(string value)
        {
            Assert.ThrowsAsync<ArgumentNullException>(() => _testClass.PublicProcessInternal(value, new HtmlDocument()));
        }

        [Test]
        public void CanCallCustomHandlerForValue()
        {
            var initialValue = "htpp://awefaefaewf&quot";
            var handler = "Url";
            var result = _testClass.PublicCustomHandlerForValue(initialValue, handler);
            Assert.AreEqual("htpp://awefaefaewf&quot", result);
        }

        [TestCase(null)]
        public void CannotCallCustomHandlerForValueWithInvalidInitialValue(string value)
        {
            Assert.Throws<ArgumentNullException>(() => _testClass.PublicCustomHandlerForValue(value, "TestValue1341799621"));
        }

        [TestCase(null)]
        public void CannotCallCustomHandlerForValueWithInvalidHandler(string value)
        {
            Assert.Throws<ArgumentNullException>(() => _testClass.PublicCustomHandlerForValue("TestValue1306282610", value));
        }
    }
}