using WebCrawler.Data.Models;

namespace WebCrawler.Core.Tests.Crawler
{
    using WebCrawler.Core.Crawler;
    using T = Book;
    using System;
    using NUnit.Framework;
    using NSubstitute;
    using WebCrawler.Core.Downloader;
    using WebCrawler.Core.Processor;
    using WebCrawler.Repository.Enum;
    using System.Threading.Tasks;
    using System.Net.Http;
    using System.Net;

    [TestFixture]
    public class CrawlerBase_1Tests
    {
        private CrawlerBase<T> _testClass;
        private IDownloader _downloader;
        private IHtmlContentProcessor _processor;
        private RepositoryType _repositoryType;
        private string _connectionString;

        [SetUp]
        public void SetUp()
        {
            _downloader = new DownloaderWithHttpClient(new HttpClient(
                new HttpClientHandler()
                {
                    AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate
                }));
            _processor = new HtmlContentProcessorPageBook();
            _repositoryType = RepositoryType.EF;
            _connectionString = "Data Source=INLT022;Initial Catalog=WebCrawler;Integrated Security=True";
            _testClass = new CrawlerBase<T>(_downloader, _processor, _repositoryType, _connectionString);
        }

        [Test]
        public void CanConstruct()
        {
            var instance = new CrawlerBase<T>(_downloader, _processor, _repositoryType, _connectionString);
            Assert.That(instance, Is.Not.Null);
        }

        [Test]
        public void CannotConstructWithNullDownloader()
        {
            Assert.Throws<ArgumentNullException>(() => new CrawlerBase<T>(default(IDownloader), Substitute.For<IHtmlContentProcessor>(), RepositoryType.EF, "TestValue676961714"));
        }

        [Test]
        public void CannotConstructWithNullProcessor()
        {
            Assert.Throws<ArgumentNullException>(() => new CrawlerBase<T>(Substitute.For<IDownloader>(), default(IHtmlContentProcessor), RepositoryType.Redis, "TestValue1084350936"));
        }

        [TestCase(null)]
        public void CannotConstructWithInvalidConnectionString(string value)
        {
            Assert.Throws<ArgumentNullException>(() => new CrawlerBase<T>(Substitute.For<IDownloader>(), Substitute.For<IHtmlContentProcessor>(), RepositoryType.EF, value));
        }

        [Test]
        public async Task CanCallProcess()
        {
            var url = "https://www.amazon.com/Tattooist-Auschwitz-Novel-Heather-Morris-ebook/dp/B0756DZ4C1/ref=pd_vtpd_14_4/130-4090834-5776767?_encoding=UTF8&pd_rd_i=B0756DZ4C1&pd_rd_r=a39cdcc4-fd41-44a1-a00e-64a759c14094&pd_rd_w=5bVh3&pd_rd_wg=BzLTU&pf_rd_p=be9253e2-366d-447b-83fa-e044efea8928&pf_rd_r=RR7V8T6SZ1831VWXMR5G&psc=1&refRID=RR7V8T6SZ1831VWXMR5G";
            var result = await _testClass.Process(url);
            Assert.AreEqual(true,result);
        }
    }
}