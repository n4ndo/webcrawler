namespace WebCrawler.Core.Tests.BackgroundTask
{
    using WebCrawler.Core.BackgroundTask;
    using System;
    using NUnit.Framework;
    using System.Threading.Tasks;

    [TestFixture]
    public class PeriodicTaskTests
    {
        private PeriodicTask _testClass;

        [SetUp]
        public void SetUp()
        {
            _testClass = new PeriodicTask();
        }

        [Test]
        public void CanConstruct()
        {
            var instance = new PeriodicTask();
            Assert.That(instance, Is.Not.Null);
        }

        [Test]
        public async Task CanCallRun()
        {
            var period = new TimeSpan();
            var result = Task.Run( async () => await PeriodicTask.Run(PeriodicCallback, period)).IsCanceled;
            Assert.AreEqual(false, result);
        }

        private Task PeriodicCallback() { PeriodicTask.Stop(PeriodicCallback); return Task.FromResult(0); }

        [Test]
        public void CannotCallRunWithNullAction()
        {
            Assert.ThrowsAsync<ArgumentNullException>(() => PeriodicTask.Run(default(Func<Task>), new TimeSpan()));
        }

        [Test]
        public void CanCallStop()
        {
            PeriodicTask.Stop(PeriodicCallback);
            Assert.Pass();
        }

        [Test]
        public void CannotCallStopWithNullAction()
        {
            Assert.Throws<ArgumentNullException>(() => PeriodicTask.Stop(default(Func<Task>)));
        }
    }
}