namespace WebCrawler.Core.Tests.Downloader
{
    using WebCrawler.Core.Downloader;
    using System;
    using NUnit.Framework;
    using System.Net.Http;
    using System.Threading.Tasks;

    [TestFixture]
    public class DownloaderWithHttpClientTests
    {
        private DownloaderWithHttpClient _testClass;
        private HttpClient _client;

        [SetUp]
        public void SetUp()
        {
            _client = new HttpClient();
            _testClass = new DownloaderWithHttpClient(_client);
        }

        [Test]
        public void CanConstruct()
        {
            var instance = new DownloaderWithHttpClient(_client);
            Assert.That(instance, Is.Not.Null);
        }

        [Test]
        public void CannotConstructWithNullClient()
        {
            Assert.Throws<ArgumentNullException>(() => new DownloaderWithHttpClient(default(HttpClient)));
        }

        [Test]
        public async Task CanCallProcess()
        {
            var url = "https://www.amazon.com/Keeper-Lost-Things-Novel-ebook/dp/B01GONIFLM/ref=pd_sim_351_6/130-4090834-5776767?_encoding=UTF8&pd_rd_i=B01GONIFLM&pd_rd_r=2bc9d271-ac49-44a7-93bd-ac7fe8326cb6&pd_rd_w=Wo9SQ&pd_rd_wg=B4D5y&pf_rd_p=9fec2710-b93d-4b3e-b3ca-e55dc1c5909a&pf_rd_r=CCVYPGD3Z7EC6D20ZJ2T&psc=1&refRID=CCVYPGD3Z7EC6D20ZJ2T";
            var result = await _testClass.Process(url);
            Assert.IsNotNull(result.DocumentNode);
        }

        [TestCase(null)]
        public void CannotCallProcessWithInvalidUrl(string value)
        {
            Assert.ThrowsAsync<ArgumentNullException>(() => _testClass.Process(value));
        }

        [Test]
        public void ClientIsInitializedCorrectly()
        {
            Assert.That(_testClass.Client, Is.EqualTo(_client));
        }
    }
}