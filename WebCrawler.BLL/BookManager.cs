﻿using System;
using System.Collections.Generic;
using System.Text;
using WebCrawler.Data.Models;
using WebCrawler.Repository;
using WebCrawler.Repository.Enum;
using WebCrawler.Repository.Factory;
using System.Linq;
using WebCrawler.BLL.ViewModel;
using WebCrawler.Utility;

namespace WebCrawler.BLL
{
    public class BookManager : BLLManager
    {
        private IRepository<Book> RepositoryBook { get; set; }
        public BookManager(RepositoryType repositoryType, string connectionString)
        {
            if (connectionString == null)
                throw new ArgumentNullException();

            RepositoryBook = RepositoryFactory<Book>.Create(repositoryType, connectionString);
        }
        public List<BookSummaryViewModel> GetAllBookSummary()
        {
            IQueryable<Book> books = RepositoryBook.GetAll();

            //var result = books.AsEnumerable() ////Force query evaluation on client 
            //                .GroupBy(s => s.Description)
            //                .Select(s => s.OrderByDescending(x => x.DateCreatedUTC).FirstOrDefault())
            //                .Select(s => new BookSummaryViewModel() {
            //                    Description = s.Description,
            //                    LatestPrice = s.Price
            //                }).ToList();

            ////Using 2 times server query evaluation (changes on EF query evaluation behaviour)
            var listSummary = books.GroupBy(s => s.Url)
                .Select(h => new { url = h.Key, maxtime = h.Max(s => s.DateCreatedUTC) }).ToList();

            var result = books.Where(s =>
                                listSummary.Select(h => h.url).Contains(s.Url) ////need to implicit conversion to ienumerable and using contains to avoid client evaluation
                                && listSummary.Select(h => h.maxtime).Contains(s.DateCreatedUTC) ////need to implicit conversion to ienumerable and using contains to avoid client evaluation
                         ).Select(s => new BookSummaryViewModel()
                         {
                             ID = s.ID,
                             Url = s.Url,
                             Description = s.Description,
                             LatestPrice = s.Price
                         }).ToList();

            return result;
        }
        public BookViewModel GetLastSuccessProcessedBook(string url)
        {
            if (url == null)
                throw new ArgumentNullException();

            IQueryable<Book> books = RepositoryBook.GetAll();
            return books.Where(s => s.Url == url).OrderByDescending(s => s.DateCreatedUTC)
                .Select(s => new BookViewModel()
                { 
                    ID = s.ID,  
                    Description = s.Description,
                    Price = s.Price,
                    UrlImage = s.UrlImage,
                    DateCreatedUTC = s.DateCreatedUTC
                }).FirstOrDefault();
        }
        public PagingModel<BookViewModel> GetBookDetailInfoByCondition(int id, int skip, int pageSize)
        {
            PagingModel<BookViewModel> result = new PagingModel<BookViewModel>();
            IQueryable<Book> books = RepositoryBook.GetAll();
            string url = books.Where(s => s.ID == id).Select(s => s.Url).FirstOrDefault();
            var records = books.Where(s => s.Url == url)
                .Select(h => new BookViewModel(){
                    ID = h.ID,
                    Description = h.Description,
                    UrlImage = h.UrlImage,
                    Price = h.Price,
                    DateCreatedUTC = h.DateCreatedUTC
                }).ToList();

            records = records.OrderByDescending(s => s.DateCreatedUTC).ToList();
            //total number of rows count   
            result.countTotal = records.Count();
            //Paging   
            result.Data = records.Skip(skip).Take(pageSize).ToList();
            return result;
        }
        public PagingModel<BookSummaryViewModel> GetAllBookSummaryByCondition(string searchValue, string sortColumn, string sortColumDirection, int skip, int pageSize)
        {
            if(searchValue == null)
            {
                throw new ArgumentNullException();
            }

            searchValue = searchValue.ToLower();
            PagingModel<BookSummaryViewModel> result = new PagingModel<BookSummaryViewModel>();
            List<BookSummaryViewModel> listCandidate = GetAllBookSummary();

            //Search evaluation on client 
            if (!string.IsNullOrEmpty(searchValue))
            {
                listCandidate = listCandidate.Where(m => m.Url.ToString().ToLower().Contains(searchValue)
                        || m.Description.ToLower().Contains(searchValue)
                        || m.LatestPrice.ToLower().Contains(searchValue)
                    ).ToList();
            }

            if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumDirection)))
            {
                listCandidate = listCandidate.OrderByField(sortColumn, sortColumDirection == "asc" ? false : true).ToList();
            }

            //total number of rows count   
            result.countTotal = listCandidate.Count();
            //Paging   
            result.Data = listCandidate.Skip(skip).Take(pageSize).ToList();
            return result;

        }
        public List<PageBookViewModel> GetAllLastExecutionTime()
        {
            IQueryable<Book> books = RepositoryBook.GetAll();
            return books.GroupBy(s => s.Url).Select(s => new PageBookViewModel() { 
                Url = s.Key, LastExecutedTimeUTC = s.Max(h => h.DateCreatedUTC) }).ToList();
        }
    }
}
