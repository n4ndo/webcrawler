﻿using System;
using System.Collections.Generic;
using System.Text;
using WebCrawler.Data.Models;

namespace WebCrawler.BLL.ViewModel
{
    public class BookSummaryViewModel
    {
        public int ID { get; set; }
        public string Url { get; set; }
        public string Description { get; set; }
        public string LatestPrice { get; set; }
    }
}
