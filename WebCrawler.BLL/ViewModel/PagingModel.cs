﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebCrawler.BLL.ViewModel
{
    public class PagingModel<T>
    {
        public List<T> Data { get; set; }
        public int countTotal { get; set; }
    }
}
