﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebCrawler.BLL.ViewModel
{
    public class PageBookViewModel
    {
        public string Url { get; set; }
        public DateTime LastExecutedTimeUTC { get; set; }
    }
}
