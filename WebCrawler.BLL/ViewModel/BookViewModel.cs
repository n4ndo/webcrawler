﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebCrawler.BLL.ViewModel
{
    public class BookViewModel
    {
        public int ID { get; set; }
        public string Description { get; set; }
        public string UrlImage { get; set; }
        public string Price { get; set; }
        public DateTime DateCreatedUTC { get; set; }
    }
}
