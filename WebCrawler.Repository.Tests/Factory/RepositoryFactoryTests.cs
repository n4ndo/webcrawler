using WebCrawler.Data.Models;

namespace WebCrawler.Repository.Tests.Factory
{
    using WebCrawler.Repository.Factory;
    using T = Book;
    using System;
    using NUnit.Framework;
    using WebCrawler.Repository.Enum;

    [TestFixture]
    public class RepositoryFactoryTests
    {
        private RepositoryFactory _testClass;

        [SetUp]
        public void SetUp()
        {
            _testClass = new RepositoryFactory();
        }

        [Test]
        public void CanConstruct()
        {
            var instance = new RepositoryFactory();
            Assert.That(instance, Is.Not.Null);
        }

        [Test]
        public void CanCallCreate()
        {
            var type = RepositoryType.Redis;
            var connectionString = "Data Source=INLT022;Initial Catalog=WebCrawler;Integrated Security=True";
            var result = RepositoryFactory.Create<T>(type, connectionString);
            Assert.AreEqual(result.GetType(), typeof(RepositoryWithRedis<T>));
        }

        [TestCase(null)]
        public void CannotCallCreateWithInvalidConnectionString(string value)
        {
            Assert.Throws<ArgumentNullException>(() => RepositoryFactory.Create<T>(RepositoryType.EF, value));
        }
    }
}