using WebCrawler.Data.Models;

namespace WebCrawler.Repository.Tests
{
    using WebCrawler.Repository;
    using T = Book;
    using System;
    using NUnit.Framework;
    using System.Threading.Tasks;

    [TestFixture]
    public class RepositoryWithEF_1Tests
    {
        private RepositoryWithEF<T> _testClass;
        private string _connetionString;

        [SetUp]
        public void SetUp()
        {
            _connetionString = "Data Source=INLT022;Initial Catalog=WebCrawler;Integrated Security=True";
            _testClass = new RepositoryWithEF<T>(_connetionString);
        }

        [Test]
        public void CanConstruct()
        {
            var instance = new RepositoryWithEF<T>(_connetionString);
            Assert.That(instance, Is.Not.Null);
        }

        [TestCase(null)]
        public void CannotConstructWithInvalidConnetionString(string value)
        {
            Assert.Throws<ArgumentNullException>(() => new RepositoryWithEF<T>(value));
        }

        [Test]
        public async Task CanCallAdd()
        {
            var data = new T() { Url = "weawefawef", UrlImage = "awefae", Description = "test", DateCreatedUTC = DateTime.UtcNow };
            var result = await _testClass.Add(data);
            Assert.AreEqual(true, result);
        }

        [Test]
        public void CannotCallAddWithNullData()
        {
            Assert.ThrowsAsync<ArgumentNullException>(() => _testClass.Add(default(T)));
        }

        [Test]
        public async Task CanCallEdit()
        {
            var id = 23;
            var data = new T() { ID = 23, DateCreatedUTC = DateTime.UtcNow, Description = "testos", Price = "999", Url = "", UrlImage = "" };
            var result = await _testClass.Edit(id, data);
            Assert.AreEqual(true, result);
        }

        [Test]
        public void CannotCallEditWithNullData()
        {
            Assert.ThrowsAsync<ArgumentNullException>(() => _testClass.Edit(9846470, default(T)));
        }

        [Test]
        public void CanCallGetAll()
        {
            var result = _testClass.GetAll();
            Assert.IsNotNull(result);
        }

        [Test]
        public async Task CanCallGetByID()
        {
            var id = 23;
            var result = await _testClass.GetByID(id);
            Assert.AreEqual(23, result.ID);
        }
    }
}