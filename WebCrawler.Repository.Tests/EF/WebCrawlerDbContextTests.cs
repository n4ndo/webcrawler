namespace WebCrawler.Repository.Tests.EF
{
    using WebCrawler.Repository.EF;
    using System;
    using NUnit.Framework;
    using Microsoft.EntityFrameworkCore;
    using WebCrawler.Data.Models;

    [TestFixture]
    public class WebCrawlerDbContextTests
    {
        private WebCrawlerDbContext _testClass;
        private string _connetionString;
        private DbContextOptions<WebCrawlerDbContext> _options;

        [SetUp]
        public void SetUp()
        {
            _connetionString = "TestValue2003431970";
            _options = new DbContextOptions<WebCrawlerDbContext>();
            _testClass = new WebCrawlerDbContext(_connetionString);
        }

        [Test]
        public void CanConstruct()
        {
            var instance = new WebCrawlerDbContext(_connetionString);
            Assert.That(instance, Is.Not.Null);
            instance = new WebCrawlerDbContext(_options);
            Assert.That(instance, Is.Not.Null);
        }

        [Test]
        public void CannotConstructWithNullOptions()
        {
            Assert.Throws<ArgumentNullException>(() => new WebCrawlerDbContext(default(DbContextOptions<WebCrawlerDbContext>)));
        }

        [TestCase(null)]
        public void CannotConstructWithInvalidConnetionString(string value)
        {
            Assert.Throws<ArgumentNullException>(() => new WebCrawlerDbContext(value));
        }
    }
}