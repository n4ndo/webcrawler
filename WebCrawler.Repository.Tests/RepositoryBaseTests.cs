using WebCrawler.Data.Models;

namespace WebCrawler.Repository.Tests
{
    using WebCrawler.Repository;
    using T = Book;
    using System;
    using NUnit.Framework;
    using System.Threading.Tasks;
    using System.Linq;

    [TestFixture]
    public class RepositoryBase_1Tests
    {
        private class TestRepositoryBase : RepositoryBase<T>
        {
            public override Task<bool> Add(T data)
            {
                return default(Task<bool>);
            }

            public override Task<bool> Edit(int id, T data)
            {
                return default(Task<bool>);
            }

            public override IQueryable<T> GetAll()
            {
                return default(IQueryable<T>);
            }

            public override Task<T> GetByID(int id)
            {
                return default(Task<T>);
            }
        }

        private TestRepositoryBase _testClass;

        [SetUp]
        public void SetUp()
        {
            _testClass = new TestRepositoryBase();
        }

        [Test]
        public void CanConstruct()
        {
            var instance = new TestRepositoryBase();
            Assert.That(instance, Is.Not.Null);
        }
    }
}