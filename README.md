# WebCrawler

#Deployment Step
1. Run db script inside DBDeploymentScript.
2. Run publishcommand.bat
3. publishcommand.bat will create folder "publish".
![](./publishfolder.JPG)
4. Copy files inside folder "publish" to server.
5. Make sure server has necessary depedency to run .net core 3.1

#Public Hosted Sample Application
<br/>
http://webcrawler.somee.com/
<br/>
http://www.webcrawler.somee.com/

#Test Step
1. input url to the textbox to start crawling the page
![](./starttest.JPG)
2. add sample link to be 
crawled (ex: https://www.amazon.com/Tattooist-Auschwitz-Novel-Heather-Morris-ebook/dp/B0756DZ4C1/ref=pd_vtpd_14_4/130-4090834-5776767?_encoding=UTF8&pd_rd_i=B0756DZ4C1&pd_rd_r=a39cdcc4-fd41-44a1-a00e-64a759c14094&pd_rd_w=5bVh3&pd_rd_wg=BzLTU&pf_rd_p=be9253e2-366d-447b-83fa-e044efea8928&pf_rd_r=RR7V8T6SZ1831VWXMR5G&psc=1&refRID=RR7V8T6SZ1831VWXMR5G)
