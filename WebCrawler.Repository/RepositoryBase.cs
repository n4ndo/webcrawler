﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebCrawler.Data;

namespace WebCrawler.Repository
{
    public abstract class RepositoryBase<T> : IRepository<T> where T : IModel
    {
        public abstract Task<bool> Add(T data);
        public abstract Task<bool> Edit(int id, T data);
        public abstract IQueryable<T> GetAll();
        public abstract Task<T> GetByID(int id);
    }
}
