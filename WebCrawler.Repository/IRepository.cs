﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebCrawler.Data;

namespace WebCrawler.Repository
{
    public interface IRepository<T> where T : IModel
    {
        Task<bool> Add(T data);
        Task<bool> Edit(int id,T data);
        Task<T> GetByID(int id);
        IQueryable<T> GetAll();
    }
}
