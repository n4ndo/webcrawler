﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebCrawler.Data;
using WebCrawler.Repository.EF;

namespace WebCrawler.Repository
{
    public class RepositoryWithEF<T> : RepositoryBase<T> where T : class,IModel
    {
        private readonly WebCrawlerDbContext _DbContext;
        public RepositoryWithEF(string connetionString) : base()
        {
            _DbContext = new WebCrawlerDbContext(connetionString);
            //_DbContext.Database.EnsureCreated();
            //_DbContext.Database.Migrate();
        }
        public override async Task<bool> Add(T data)
        {
            if (data == null)
            {
                throw new ArgumentNullException();
            }
            await _DbContext.Set<T>().AddAsync(data).ConfigureAwait(false);
            await _DbContext.SaveChangesAsync().ConfigureAwait(false);
            return true;
        }

        public override async Task<bool> Edit(int id, T data)
        {
            if (data == null)
            {
                throw new ArgumentNullException();
            }
            _DbContext.Set<T>().Update((T)data);
            await _DbContext.SaveChangesAsync().ConfigureAwait(false);
            return true;
        }

        public override IQueryable<T> GetAll()
        {
            return _DbContext.Set<T>().AsNoTracking();
            //return Task.FromResult(_DbContext.Set<T>().AsNoTracking());
        }

        public override async Task<T> GetByID(int id)
        {
            return await _DbContext.Set<T>().AsNoTracking()
                .FirstOrDefaultAsync(s => s.ID == id).ConfigureAwait(false);
        }
    }
}
