﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebCrawler.Repository.Enum
{
    public enum RepositoryType
    {
        EF = 1,
        Redis = 2
    }
}
