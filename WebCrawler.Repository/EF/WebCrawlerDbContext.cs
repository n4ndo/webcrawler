﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using WebCrawler.Data.Models;

namespace WebCrawler.Repository.EF
{
    public partial class WebCrawlerDbContext : DbContext
    {
        private string _connectionString;
        public WebCrawlerDbContext(string connetionString) {

            if(connetionString == null)
            {
                throw new ArgumentNullException();
            }
            _connectionString = connetionString;
        }
        public WebCrawlerDbContext(DbContextOptions<WebCrawlerDbContext> options) : base(options) { }
        public virtual DbSet<Book> Books { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if(!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_connectionString);
            }
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.HasSequence("book_hilo").IncrementsBy(10);

            modelBuilder.Entity<Book>(entity =>
            {
                entity.Property(h => h.ID).ValueGeneratedOnAdd();
                entity.Property(h => h.Description).HasMaxLength(200);
                entity.Property(h => h.UrlImage).HasMaxLength(500);
                entity.Property(h => h.Price).HasMaxLength(20);
                entity.Property(h => h.DateCreatedUTC);
                entity.Property(h => h.Url).HasMaxLength(500);
            });
        }
    }
}
