﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebCrawler.Repository.Migrations
{
    public partial class CreateBookTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Books",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Description = table.Column<string>(maxLength: 200, nullable: true),
                    Url = table.Column<string>(maxLength: 500, nullable: true),
                    UrlImage = table.Column<string>(maxLength: 500, nullable: true),
                    Price = table.Column<string>(maxLength: 20, nullable: true),
                    DateCreatedUTC = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Books", x => x.ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Books");
        }
    }
}
