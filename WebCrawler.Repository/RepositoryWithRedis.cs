﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebCrawler.Data;

namespace WebCrawler.Repository
{
    public class RepositoryWithRedis<T> : RepositoryBase<T> where T : IModel
    {
        public RepositoryWithRedis(string connectionString) : base()
        {
        }
        public override Task<bool> Add(T data)
        {
            if (data == null)
            {
                throw new ArgumentNullException();
            }

            throw new NotImplementedException();
        }

        public override Task<bool> Edit(int id, T data)
        {
            if (data == null)
            {
                throw new ArgumentNullException();
            }

            throw new NotImplementedException();
        }

        public override IQueryable<T> GetAll()
        {
            throw new NotImplementedException();
        }

        public override Task<T> GetByID(int id)
        {
            throw new NotImplementedException();
        }
    }
}
