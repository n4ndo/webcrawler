﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using WebCrawler.Data;
using WebCrawler.Repository.Enum;

namespace WebCrawler.Repository.Factory
{
    public class RepositoryFactory<T> where T : class,IModel
    {
        private static ConcurrentDictionary<string, IRepository<T>> repoCollection = new ConcurrentDictionary<string, IRepository<T>>();

        public static IRepository<T> Create(RepositoryType type, string connectionString)
        {
            IRepository<T> repo = null;

            if (repoCollection.ContainsKey(type.ToString()))
            {
                repoCollection.TryGetValue(type.ToString(), out repo);
            }
            else
            {
                switch (type)
                {
                    case RepositoryType.EF:
                        repo = new RepositoryWithEF<T>(connectionString);
                        break;
                    case RepositoryType.Redis:
                        repo = new RepositoryWithRedis<T>(connectionString);
                        break;
                }
                repoCollection.TryAdd(type.ToString(), repo);
            }

            return repo;
        }
    }
}
